This directory has uboot( uboot.img ) and loader( RK3126MiniLoaderAll_Vxxx.bin ).

Originally installed uboot and loader seem to have some compatibility problem
  with Rockchip's Linux_Upgrade_Tool, especially with writing operation( like 'wl' command ) .

Replacing the original files with ones in this directory might solve the problem.
But do it at your own risk.


The code is based on the 'u-boot' directory of Firefly's source.
    
note:
 Firefly's source is available at
   wiki.t-firefly.com/en/Firefly-RK3128/Starter_Guide.html.
   ( git repogitory -- https://drive.google.com/drive/folders/1A9bkkJAKieTeUdPhTqGQbLD4nwMJr6_W )
