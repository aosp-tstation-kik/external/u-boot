#!/bin/bash

# FILE <-- xxx.ini
RKCHIP=${1:-RK3126}
RKCHIP_UP=${RKCHIP^^[a-z]}
FILE="./tools/rk_tools/RKBOOT/${RKCHIP_UP}MINIALL.ini"


# MAJOR, MINOR, PATH <-- ${FILE}
MAJOR=`cat ${FILE} | grep 'MAJOR=[0-9]*' | grep -o '[0-9]*'`
MINOR=`cat ${FILE} | grep 'MINOR=[0-9]*' | grep -o '[0-9]*'`
PATH=`cat ${FILE} | grep 'PATH=.*' | grep -o '[^=]*$' | grep -o '^.*[^.]'`


# xxxx_V<MAJOR>.<MINOR>.bin <-- xxxx.bin ( xxxx == basename of ${PATH} )
echo ${PATH/./_V${MAJOR}.${MINOR}.}
