LOCAL_PATH := $(call my-dir)

########################
# build uboot.img

include $(CLEAR_VARS)
LOCAL_MODULE := u-boot
LOCAL_MODULE_STEM := uboot.img
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := PACKAGING
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/uboot
include $(BUILD_SYSTEM)/base_rules.mk
#... set UBOOT_XXX
UBOOT_WORK_DIR := $(ANDROID_BUILD_TOP)/$(call local-intermediates-dir)
UBOOT_LOCAL_PATH := $(ANDROID_BUILD_TOP)/$(LOCAL_PATH)
UBOOT_HOSTCC := $(ANDROID_BUILD_TOP)/$(HOST_CC)
UBOOT_HOSTCXX := $(ANDROID_BUILD_TOP)/$(HOST_CXX)
UBOOT_CROSS_COMPILE := $(ANDROID_EABI_TOOLCHAIN)/arm-linux-androideabi-
UBOOT_RKCHIP := rk3126
UBOOT_SUFFIX := _mod_defconfig	# _defconfig
UBOOT_CONFIG := $(UBOOT_RKCHIP)$(UBOOT_SUFFIX)


# step1 ( target = $(UBOOT_CONFIG) )
#   .config is created.
#
# step2 ( target = ubootrelease )
#   A recipe to build include/config/auto.conf is enabled (line 518) ( because target != %config ).
#   include/config/auto.conf is created, and newly included (line 505).
#   So Makefiles are remade ( see manual '3.5 How Makefiles Are Remade' ).
#   When this occurs, Gnu Make ver 3.8.2 clears all variables specified at the command line.
#		So we don't go further, but go back to parent level, and proceed to step3.
#	  Without step2, u-boot is built with HOSTCC=cc ( not with HOSTCC=$(UBOOT_HOSTCC) ). 
#
# step3 ( target = all )
#   re-start make with variable overrides( HOSTCC etc ).
#
$(LOCAL_BUILT_MODULE): u-boot-readme $(UBOOT_LOCAL_PATH)/src/configs/$(UBOOT_CONFIG)
	MAKEOVERRIDES="" MAKEFLAGS="" $(MAKE) \
			 HOSTCC=$(UBOOT_HOSTCC) HOSTCXX=$(UBOOT_HOSTCXX) CROSS_COMPILE=$(UBOOT_CROSS_COMPILE) \
			 O=$(UBOOT_WORK_DIR) -C $(UBOOT_LOCAL_PATH)/src -f $(UBOOT_LOCAL_PATH)/src/Makefile \
			 $(UBOOT_CONFIG)
	MAKEOVERRIDES="" MAKEFLAGS="" $(MAKE) \
			 HOSTCC=$(UBOOT_HOSTCC) HOSTCXX=$(UBOOT_HOSTCXX) CROSS_COMPILE=$(UBOOT_CROSS_COMPILE) \
			 O=$(UBOOT_WORK_DIR) -C $(UBOOT_LOCAL_PATH)/src -f $(UBOOT_LOCAL_PATH)/src/Makefile \
			 ubootrelease
	MAKEOVERRIDES="" MAKEFLAGS="" $(MAKE) \
			 HOSTCC=$(UBOOT_HOSTCC) HOSTCXX=$(UBOOT_HOSTCXX) CROSS_COMPILE=$(UBOOT_CROSS_COMPILE) \
			 O=$(UBOOT_WORK_DIR) -C $(UBOOT_LOCAL_PATH)/src -f $(UBOOT_LOCAL_PATH)/src/Makefile \


#	MAKEOVERRIDES="" MAKEFLAGS="" $(MAKE) \
#			 HOSTCC=$(UBOOT_HOSTCC) HOSTCXX=$(UBOOT_HOSTCXX) CROSS_COMPILE=$(UBOOT_CROSS_COMPILE) \
#			 O=$(UBOOT_WORK_DIR) -C $(UBOOT_LOCAL_PATH)/src -f $(UBOOT_LOCAL_PATH)/src/Makefile \
#			 distclean




########################
# make symlink ( u-boot-loader_intermediates/<loader>.bin --> u-boot_intermediates/<loader>.bin )

include $(CLEAR_VARS)
LOCAL_MODULE := u-boot-loader
#LOCAL_MODULE_STEM := RK3126MiniLoaderAll_V2.25.bin
LOCAL_MODULE_STEM := $(shell cd $(LOCAL_PATH)/src ; ./loaderName.sh $(UBOOT_RKCHIP))
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := PACKAGING
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/uboot
include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE): u-boot
	mkdir -p $(dir $@)
	ln -fs $(UBOOT_WORK_DIR)/$(notdir $@) $@


########################
# copy readme.txt

include $(CLEAR_VARS)
LOCAL_MODULE := u-boot-readme
LOCAL_MODULE_STEM := readme.txt
LOCAL_MODULE_CLASS := SHELL_SCRIPT
LOCAL_SRC_FILES := readme.txt
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/uboot
include $(BUILD_PREBUILT)
